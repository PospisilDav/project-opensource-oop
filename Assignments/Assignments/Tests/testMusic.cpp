//
//  testMusic.cpp
//  Assignments
//
//  Created by David Pospisil on 16.02.24.
//

#include "Tests.h"
#include <list>

namespace Music {
    
    void testSong(){
        Song satisfaction("Satisfaction", 1965);
        
        assert(satisfaction.getTitle() == "Satisfaction");
        assert(satisfaction.getReleased() == 1965);
    }

    void testBand(){
        
        //******************//
        //*  test getters  *//
        //******************//
        {
            Song satisfaction("Satisfaction", 1965);
            Band band("The Rolling Stones", 1962, satisfaction);
            
            assert( band.getName() == "The Rolling Stones");
            assert( band.getFounded() == 1962 );
            
            Song mostPopular= band.getMostPopular();
            assert( mostPopular.getTitle() == "Satisfaction" );
            assert( mostPopular.getReleased() == 1965 );
        }
        
        //******************//
        //*  test compare  *//
        //******************//
        {
            Song song("something", 1999);
            Band band1("Beatles", 1963, song);
            Band band2("The Rolling Stones", 1980, song);
            
            assert(band1.compare(band2) < 0);
        }
        
        //***************//
        //*  test sort  *//
        //***************//
        {
            Song song("something", 1999);
            
            Band bands[5] = {
                Band("band4", 1904, song),
                Band("band2", 1902, song),
                Band("band5", 1905, song),
                Band("band3", 1903, song),
                Band("band1", 1901, song)
            };
            
            Band sortedBands[] = {
                Band("band1", 1901, song),
                Band("band2", 1902, song),
                Band("band3", 1903, song),
                Band("band4", 1904, song),
                Band("band5", 1905, song)
            };
            
            sort(bands, 5);
            
            for(int i=0; i<5; i++)
                assert(sortedBands[i].getName() == bands[i].getName());
        }
        
        //******************//
        //*  test members  *//
        //******************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            assert( stones.getSize() == 3 );
            assert( stones.getMember(0) == members[0] );
            assert( stones.getMember(1) == members[1] );
            assert( stones.getMember(2) == members[2] );
            members[0]="Duck";
            assert( stones.getMember(0) == "Jagger" );
            try{
                Band stones("The Rolling Stones", 1962, satisfaction, members, -1);
                assert(false);
            }catch (...){}
        }
        
        //********************//
        //*  test setMember  *//
        //********************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            stones.setMember(0,"Duck");
            assert( stones.getMember(0) == "Duck" );
            
            try{
                stones.setMember(3, "Ludwig");
                assert(false);
            } catch(...){ }
            
            try{
                stones.setMember(-1, "Ludwig");
                assert(false);
            } catch(...){ }
        }
        
        //****************************//
        //*  test Kopierkonstruktor  *//
        //****************************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            Band copy=stones;
            stones.setMember(0,"Duck");
            assert(copy.getMember(0) == "Jagger");
        }
        
        //*******************************//
        //*  test comparison-operators  *//
        //*******************************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            assert(stones==stones);
            Band copy=stones;
            assert(stones==copy);
            assert(copy==stones);
            Song ruby("Ruby Tuesday", 1967);
            Band stonesRuby("The Rolling Stones", 1962, ruby,members,3 );
            assert(stones!=stonesRuby);
            assert(stonesRuby!=stones);
            Band noWood("The Rolling Stones", 1962, satisfaction, members,2);
            assert(stones!=noWood);
            assert(noWood!=stones);
            copy.setMember(0,"Richards");
            copy.setMember(1,"Jagger");
            assert(stones!=copy);
            assert(copy!=stones);
        }
        
        //********************//
        //*  test addMember  *//
        //********************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger", "Richards", "Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction, members, 3 );
            stones.addMember("Duck");
            assert(stones.getMember(3)=="Duck");
            assert(stones.getSize() == 4);
        }
        
        //**************************************//
        //*  test output-operator (std::cout)  *//
        //**************************************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[]{"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            std::cout << stones << std::endl;
        }
        
        //**************************************//
        //*  Standard-Zuweisungsoperator Song  *//
        //**************************************//
        {
            Song satisfaction("Satisfaction", 1965);
            Song sameSong=satisfaction;
            satisfaction=sameSong;
            assert(sameSong==satisfaction);
        }
        
        //*****************************//
        //*  Zuweisungsoperator Band  *//
        //*****************************//
        {
            Song satisfaction("Satisfaction", 1965);
            std::string members[] = {"Jagger","Richards","Wood"};
            Band stones("The Rolling Stones", 1962, satisfaction,members,3 );
            Band copy = stones;
            stones = copy;
            stones.setMember(0, "Duck");
            assert(copy != stones);
        }
        
        //**********************//
        //*  test string-sort  *//
        //**********************//
        {
            std::string strings[] = {"HFU", "World", "Hello"};
            sort(strings, 3);
            assert(strings[0]=="HFU");
            assert(strings[1]=="Hello");
            assert(strings[2]=="World");
            
            std::string unsortedStrings[]{
                "Sigma",
                "Delta",
                "Gamma",
                "Beta",
                "Omega",
                "Alpha"
            };
            
            std::string sortedStrings[]{
                "Alpha",
                "Beta",
                "Delta",
                "Gamma",
                "Omega",
                "Sigma"
            };
            
            sort(unsortedStrings, 6);
            
            for(int i=0; i<6; i++)
                assert(sortedStrings[i] == unsortedStrings[i]);
        }
    }

    void testMusic(){
        testSong();
        testBand();
    }


        

}


