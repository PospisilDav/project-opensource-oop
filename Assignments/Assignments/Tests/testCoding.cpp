//
//  testCoding.cpp
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 16.02.24.
//

#include "Tests.h"
#include "../source/source.h"

void testCoding(){
    
    {
        //**********//
        //*  char  *//
        //**********//
        
        char expectedEncode[] = "afub";
        char input[] = "zeta";
        Char::encode(input);
        
        for(int i=0; input[i] != '\0'; i++)
            assert(input[i] == expectedEncode[i]);
        
        char expectedDecode[] = "zeta";
        
        Char::decode(input);
        
        for(int i=0; input[i] != '\0'; i++)
            assert(input[i] == expectedDecode[i]);
        
    }
    
    {
        //************//
        //*  string  *//
        //************//
    
        std::string expected = "afub";
        std::string input = "zeta";
        String::encode(input);
        
        assert(input == expected);
        
        expected = "zeta";
        String::decode(input);
        
        assert(input == expected);
    }
    
    {
        //*****************//
        //*  copyStrings  *//
        //*****************//
        
        const int iLength = 5;
        
        std::string* input = new std::string[iLength]();

        std::string* copiedInput = new std::string();
        
        for(int i=0; i<iLength; i++)
            input[i] = "David";
        copiedInput = String::copyStrings(input);
        
        for(int i=0; i<iLength; i++)
            assert(input[i] == copiedInput[i]);
        
        // change copied input, to proof It's not the same
        copiedInput[0] = "Peda";
        
        assert(input[0] != copiedInput[0]);
    }
}
