//
//  testMain.cpp
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 10/02/24
//

#include <iostream>
#include "Tests.h"

void testMain(){
    testPrime();
    testCoding();
    Music::testMusic();
}
