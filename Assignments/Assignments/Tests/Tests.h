//
//  Tests.h
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 10/02/24
//

#ifndef Tests_h
#define Tests_h

#include <iostream>
#include <assert.h>
#include "../source/source.h"

void testMain();

void testPrime();

void testCoding();

namespace Music {
    void testMusic();
}

#endif /* Tests_h */
