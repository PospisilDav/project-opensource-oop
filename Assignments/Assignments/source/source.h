//
//  Cpp.h
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 10.02.24.
//

#ifndef Cpp_h
#define Cpp_h

#include <iostream>

//*****************//
//*  Task 03, 04  *//
//*****************//

namespace Music {

    class Song {
    private:
        std::string strTitle;
        int iReleaseYear;
    public:
        Song(const std::string& title, int release);
        const std::string& getTitle() const;
        int getReleased() const;
        
        bool operator==(const Song& other);
        bool operator!=(const Song& other);
        friend std::ostream& operator<<(std::ostream& out, const Song& other);
    };

    class Band {
    private:
        std::string strName;
        int iFounded;
        Song mostPopularSong;
        std::string* members;
        int bandSize;
        
        std::string* copyArrayToMembers(std::string* members, int bandSize, const bool increaseBandSize=false) const;
        int initBandSize(int bandSize);
    public:
        Band(const std::string& name, int found, const Song& mostPopular, std::string* members, const int bandSize);
        Band(const std::string& name, int found, const Song& mostPopular);
        Band(const Band& other);
        int compare(const Band& other) const;
        
        void addMember(const std::string& name);
        void setMember(int i, const std::string& name);
        
        std::string getName() const;
        int getFounded() const;
        Song getMostPopular() const;
        std::string getMember(int index) const;
        int getSize() const;
        
        bool operator==(const Band& other);
        bool operator!=(const Band& other);
        friend std::ostream& operator<<(std::ostream& out, const Band& other);
        Band& operator=(Band& other);
        
        ~Band();
    };

    //**********//
    //*  sort  *//
    //**********//

    template <typename T> void swap(T& t1, T& t2){
        T t3 = t1;
        t1 = t2;
        t2 = t3;
    }

    template <typename T> void sort(T* array, const int length){
        if (array == nullptr)
            throw std::invalid_argument("The bands array pointer is null");

        if (length < 0)
            throw std::invalid_argument("Invalid negative length of bands array");
        
        for(int i=0; i<length-1; i++)
            for(int j=0; j<length-i-1; j++){
                if(array[j].compare(array[j+1]) > 0)
                    Music::swap(array[j], array[j+1]);
        }
    }
}

//*****************//
//*  Task 01, 02  *//
//*****************//

bool isPrime(int n);

namespace Char {
    void encode(char* text);
    void decode(char* text);
}

namespace String {
    void encode(std::string& text);
    void decode(std::string& text);

    std::string* copyStrings(std::string* text);
}

#endif /* Cpp_h */
