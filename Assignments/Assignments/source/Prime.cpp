//
//  Prime.cpp
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 10/02/24
//

#include "source.h"

bool isPrime(int n){
    for(int i=2; i<n; i++)
        if(n % i == 0)
            return false;
    return true;
}
