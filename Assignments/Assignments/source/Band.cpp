//
//  Band.cpp
//  Assignments
//
//  Created by David Pospisil on 16.02.24.
//

#include "source.h"

namespace Music {

    Band::Band(const std::string& name, int found, const Song& mostPopular, std::string* members, const int bandSize): strName(name), iFounded(found), mostPopularSong(mostPopular), members(copyArrayToMembers(members, initBandSize(bandSize))){}

    Band::Band(const std::string& name, int found, const Song& mostPopular): Band(name, found, mostPopular, new std::string[0]{}, 0){};

    Band::Band(const Band& other): Band(other.strName, other.iFounded, other.mostPopularSong, other.members, other.bandSize){};

    int Band::initBandSize(int bandSize) {
        if(bandSize < 0)
            new std::invalid_argument("invalid bandSize parameter");
        this->bandSize = bandSize;
        return bandSize;
    }

    void Band::addMember(const std::string& name){
        
        std::string* oldMembers = copyArrayToMembers(members, bandSize, true);
        
        delete[] members;
        
        members = new std::string[bandSize++];
        
        members = oldMembers;
        members[bandSize-1] = name;
    }

    void Band::setMember(int i, const std::string& name){
        if(i < 0 || i >= bandSize)
            throw std::invalid_argument("index out of bounce!");
        if(bandSize > 0)
            members[i] = name;
    }

    std::string Band::getName() const {
        return strName;
    }

    int Band::getFounded() const {
        return iFounded;
    }
        
    Song Band::getMostPopular() const {
        return mostPopularSong;
    }

    std::string Band::getMember(int index) const{
        if(index >= bandSize || index < 0)
            new std::invalid_argument("index out of bounce!");
        return members[index];
    }

    int Band::getSize() const {
        return bandSize;
    }

    int Band::compare(const Band& other) const {
        return this->iFounded - other.iFounded;
    }

    std::string* Band::copyArrayToMembers(std::string* members, int bandSize, const bool increaseBandSize) const {
        
        if(bandSize < 0)
            new std::invalid_argument("invalid bandSize");
        if(members == nullptr)
            new std::invalid_argument("invalid members");
        
        std::string* newMembers = new std::string[bandSize];
        
        if(increaseBandSize){
            delete[] newMembers;
            newMembers = new std::string[bandSize+1];
        }
        
        for(int i=0; i<bandSize; i++)
            newMembers[i] = members[i];
        
        return newMembers;
    }

    bool Band::operator==(const Band& other){
        if(strName != other.strName) return false;
        if(iFounded != other.iFounded) return false;
        if(mostPopularSong != other.mostPopularSong) return false;
        if(bandSize != other.bandSize) return false;
        for(int i=0; i<bandSize; i++)
            if(members[i] != other.members[i]) return false;
        return true;
    }

    bool Band::operator!=(const Band& other){
        return !(*this == other);
    }

    std::ostream& operator<<(std::ostream& out, const Band& other){
        out << "name: " << other.strName << ", founded: " << other.iFounded << ", members: ";
        for(int i=0; i<other.bandSize; i++)
            out << other.members[i] << ", ";
        out << "most popular song: " << other.mostPopularSong;
        return out;
    }

    Band& Band::operator=(Band& other){
        if(&other == this)
            return other;
        
        strName = other.strName;
        iFounded = other.iFounded;
        mostPopularSong = other.mostPopularSong;
        members = copyArrayToMembers(other.members, other.bandSize);
        bandSize = other.bandSize;
        
        return *this;
    }

    Band::~Band(){
        if(bandSize > 0)
            delete[] this->members;
    }
}
