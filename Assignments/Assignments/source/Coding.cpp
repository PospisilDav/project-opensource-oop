//
//  Coding.cpp
//  Project OpenSource-OOP
//
//  Created by David Pospisil on 16.02.24.
//

#include "source.h"

namespace Char {

void code(char* text, int direction){
    
    int iLimit = (direction == -1)? 97 : 122;
    int iSource = (direction == -1)? 122 : 97;
    
    for(int i=0; text[i] != '\0'; i++){
        if(text[i] > (122) || text[i] < 97)
            throw std::invalid_argument("text is only lowerCase letters");
        text[i] = (text[i] == iLimit)? iSource : text[i]+direction;
    }
}

void encode(char* text){
    code(text, 1);
}

void decode(char* text){
    code(text, -1);
}

}

namespace String {

void code(std::string& text, int direction){
    char* cText = new char[text.length()]();
    
    strcpy(cText, text.c_str());
    
    Char::code(cText, direction);
    
    text = cText;
    
    delete[] cText;
}

void encode(std::string& text){
    code(text, 1);
}

void decode(std::string& text){
    code(text, -1);
}

std::string* copyStrings(std::string* text){
    
    std::string* copyText = new std::string();
    
    for(int i=0; text[i] != "\0"; i++)
        copyText[i] = text[i];
    
    return copyText;
}

}
