//
//  Song.cpp
//  Assignments
//
//  Created by David Pospisil on 16.02.24.
//

#include "source.h"

namespace Music {
    Song::Song(const std::string& title, int release): strTitle(title), iReleaseYear(release){};

    const std::string& Song::getTitle() const {
        return strTitle;
    }

    int Song::getReleased() const {
        return iReleaseYear;
    }

    bool Song::operator==(const Song& other){
        if(strTitle != other.strTitle) return false;
        if(iReleaseYear != other.iReleaseYear) return false;
        return true;
    }

    bool Song::operator!=(const Song& other){
        return !(*this == other);
    }

    std::ostream& operator<<(std::ostream& out, const Song& other){
        out << "title: " << other.strTitle << ", released: " << other.iReleaseYear;
        return out;
    }
}
